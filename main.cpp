///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   3/20/2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <unistd.h>
#include <array>
#include <list>


#include "animal.hpp"

#define FARM_SIZE 30


using namespace std;
using namespace animalfarm;


int main() {
   cout << "Welcome to Animal Farm 3!" << endl;

   srand (time(0));

   array<Animal*, FARM_SIZE> animalArray;
   animalArray.fill( NULL );
   for (int i = 0 ; i < FARM_SIZE ; i++ ) {
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }

   cout << std::boolalpha;
   cout << endl << "Array of Animals: " << endl;
   cout << "   Is it empty: " << animalArray.empty() << endl;
   cout << "   Number of elements: " << animalArray.size() << endl;
   cout << "   Max size: " << animalArray.max_size() << endl;

   for( Animal* animal : animalArray ) {
      cout << animal -> speak() << endl;
   }

   for( Animal* animal : animalArray ) {
      delete animal;
   }


   list<Animal*> animalList;
   for (int i = 0 ; i < 25 ; i++ ) {
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }

   cout << std::boolalpha;
   cout << endl << "List of Animals: " << endl;
   cout << "   Is it empty: " << animalList.empty() << endl;
   cout << "   Number of elements: " << animalList.size() << endl;
   cout << "   Max size: " << animalList.max_size() << endl;

   for( Animal* animal : animalList ) {
      cout << animal -> speak() << endl;
   }

   for( Animal* animal : animalList ) {
      delete animal;
   }

   return 0;
}
