///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/17/2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool Native, enum Color newColor, enum Gender newGender ) {
   gender = newGender;         /// Get from the constructor... not all cats are the same gender (this is a has-a relationship)
   species = "Fistularia chinensis";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
   scaleColor = newColor;       /// A has-a relationship, so it comes through the constructor
   favoriteTemp = 80.6;    /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period
   isNative = Native;
}




/// Print our Cat and name first... then print whatever information Mammal holds.
void Nunu::printInfo() {
   cout << "Nunu" << endl;
   std::cout << "   Is native = ";
   std::cout << std::boolalpha << isNative << endl;
   Fish::printInfo();
}

} 
